#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace std::chrono_literals;

struct Sync : ostringstream {
    ostream& os;

    explicit Sync(ostream& os) : os{os} {}

    ~Sync() override {
        (*this) << "\n";
        os << ostringstream::str();
        os.flush();
    }
};

string operator*(const string& s, unsigned n) {
    string r;
    while (n-- > 0) r += s;
    return r;
}

int main(int argc, char** argv) {
    auto numThreads  = 10U;
    auto numMessages = 25U;

    for (int k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-t"s) numThreads = stoi(argv[++k]);
        else if (arg == "-m"s) numMessages = stoi(argv[++k]);
    }

    auto body = [numMessages](unsigned id) {
        for (auto i = 0U; i < numMessages; ++i) {
            Sync{cout} << ("    "s * id) << "[" << id << "]";
            this_thread::sleep_for(1s);
        }
    };

    vector<thread> threads;
    threads.reserve(numThreads);
    for (auto k = 0U; k < numThreads; ++k) {
        threads.emplace_back(body, k + 1);
    }
    for (auto& t: threads) t.join();
}
