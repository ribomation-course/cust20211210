#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>
#include "file.hxx"

namespace ribomation::io {
    using namespace std;

    File::File(const string& filename)
       : fd{ open(filename.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644) }
    {
        if (fd == -1) {
            throw runtime_error{"open failed: "s + strerror(errno)};
        }
    }

    File::~File() {
        close(fd);
    }

    void File::print(const string& str) const {
        unsigned long cnt = ::write(fd, str.c_str(), str.size());
        if (cnt != str.size()) {
            throw runtime_error{"print failed: "s + strerror(errno)};
        }
    }

    void File::println(const string& str) const {
        print(str + "\n"s);
    }

}
