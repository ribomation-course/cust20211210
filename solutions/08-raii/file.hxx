#pragma once
#include <string>

namespace ribomation::io {
    using std::string;

    class File {
        const int fd;

    public:
        explicit File(const string& filename);
        ~File();
        void print(const string& str) const;
        void println(const string& str) const;
    };

}
