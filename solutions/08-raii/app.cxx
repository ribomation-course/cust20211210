#include <iostream>
#include <filesystem>
#include "file.hxx"
using namespace std;
using namespace ribomation::io;
namespace fs = std::filesystem;

int main() {
    auto filename = fs::path{"./tjabba.txt"s};
    {
        auto f = File{filename};
        f.println("Hello from a tiny C++ object"s);
        f.println("This is the 2nd line"s);
        f.println("Finally, the last line is here"s);
    }
    auto numBytes = fs::file_size(filename);
    cout << "written " << numBytes << " bytes to " << filename << endl;
}
