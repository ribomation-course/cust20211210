#include <iostream>
#include <list>
#include <set>
#include <unordered_set>
#include <random>
#include "trace-alloc.hxx"
using namespace std;

template<typename T>
void use_list(int N) {
    cout << "-- std::list<T>:" << __PRETTY_FUNCTION__ << ": sizeof " << sizeof(T) << " bytes\n";
    auto mgr     = AllocManager{};
    auto numbers = list<T>{};

    for (auto k = 1; k <= N; ++k) numbers.push_back(k);
    for (auto k: numbers) cout << k << " ";
    cout << "\n";
}

template<typename T>
void use_ordered_set(int N) {
    cout << "-- std::set<T>:" << __PRETTY_FUNCTION__ << ": sizeof " << sizeof(T) << " bytes\n";
    auto mgr     = AllocManager{};
    auto numbers = set<T>{};

    auto rect    = uniform_int_distribution{1, 100};
    auto r = default_random_engine{1234567};

    for (auto k = 1; k <= N; ++k) numbers.insert(rect(r));
    for (auto k: numbers) cout << k << " ";
    cout << "\n";
}

template<typename T>
void use_unordered_set(int N) {
    cout << "-- std::set<T>:" << __PRETTY_FUNCTION__ << ": sizeof " << sizeof(T) << " bytes\n";
    auto mgr     = AllocManager{};
    auto numbers = unordered_set<T>{};

    auto rect    = uniform_int_distribution{1, 100};
    auto r = default_random_engine{1234567};

    for (auto k = 1; k <= N; ++k) numbers.insert(rect(r));
    for (auto k: numbers) cout << k << " ";
    cout << "\n";
}


int main() {
    auto const N = 10;

    use_list<short>(N);
    use_list<int>(N);
    use_list<long>(N);

    use_ordered_set<int>(N);
    use_unordered_set<int>(N);
}
