#include <iostream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <sys/resource.h>
//using namespace std;

// run it with:
// ltrace -S ./name-of-app
// ltrace -Sc ./name-of-app

int main(int argc, char** argv) {
    using std::string;
    using std::runtime_error;
    using namespace std::string_literals;

    bool verbose = (argc == 1) || (string{argv[1]} == "verbose"s);

    auto          max_vm = (1L << 24);
    struct rlimit rlim{};
    rlim.rlim_cur = max_vm;
    rlim.rlim_max = max_vm;
    if (setrlimit(RLIMIT_AS, &rlim) < 0) {
        throw runtime_error{"setrlimit failed: "s + strerror(errno)};
    }

    auto const size  = 1 << 10;
    auto       count = 0;
    for (void* ptr = nullptr; (ptr = malloc(size)) != nullptr; ++count) {
        if (verbose) printf("%d) %p\n", count+1, ptr);
    }

    printf("allocated %d blocks of size %d\n", count, size);
    printf("total %d bytes\n", (count * size));
}
