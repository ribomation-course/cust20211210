#include <iostream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <malloc.h>

using namespace std;

int main() {
    auto const threshold = 32 * 1024;
    if (mallopt(M_MMAP_THRESHOLD, threshold) < 0) {
        throw invalid_argument{"mallopt failed: "s + strerror(errno)};
    }

    auto below = malloc(threshold - 8);
    auto above = malloc(threshold + 8);
    cout << "&below: " << below << "\n";
    cout << "&above: " << above << "\n";
    cout << "diff: " << (reinterpret_cast<unsigned long>(above) - reinterpret_cast<unsigned long>(below)) << "\n";
}
