#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <stdexcept>

namespace ribomation {
    using namespace std;
    using namespace std::string_literals;

    inline auto count(const string& filename) -> tuple<unsigned, unsigned, unsigned> {
        auto f = ifstream{filename};
        if (!f) throw invalid_argument("cannot open "s + filename);

        auto lines = 0U;
        auto words = 0U;
        auto chars = 0U;

        for (string line; getline(f, line); ++lines) {
            chars += line.size();
            auto buf = istringstream{line};
            for (string   w; buf >> w; ++words);
        }

        return make_tuple(lines, words, chars);
    }

}
