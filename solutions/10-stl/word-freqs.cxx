#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <cctype>

using namespace std;
using namespace std::chrono;

auto strip(string word) -> string;
auto lc(string word) -> string;
auto load(istream& in) -> unordered_multiset<string>;
void print(const unordered_multiset<string>&);

int main(int argc, char** argv) {
    auto startTime = high_resolution_clock::now();

    auto filename = (argc == 1) ? "../musketeers.txt"s : argv[1];
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto words = load(file);
    cout << "# words: " << words.size() << "\n";
    print(words);

    auto endTime = high_resolution_clock::now();
    auto elapsed = duration_cast<milliseconds>(endTime - startTime);
    cout << "----\nelapsed " << elapsed.count() << " ms\n";
}

auto load(istream& in) -> unordered_multiset<string> {
    auto words = unordered_multiset<string>{};

    for (string word; in >> word;) {
        word = strip(word);
        if (word.size() >= 4) words.insert(lc(word));
    }

    return words;
}

auto lc(string word) -> string {
    transform(word.begin(), word.end(), word.begin(), [](auto ch) {
        return tolower(ch);
    });
    return word;
}

auto strip(string word) -> string {
    auto result = ""s;
    copy_if(word.begin(), word.end(), back_inserter(result), [](auto ch) {
        return isalpha(ch);
    });
    return result;
}

void print(const unordered_multiset<string>& words) {
    auto freqs = multiset<string>{words.begin(), words.end()};

    for (auto it = freqs.begin(); it != freqs.end();) {
        auto word = *it;
        auto freq = freqs.count(word);
        cout << word << ": " << freq << endl;
        advance(it, freq);
    }
}