#pragma once
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <unordered_set>

namespace ribomation::io {
    using namespace std;
    namespace fs = std::filesystem;

    struct Count {
        const string name;
        unsigned     lines = 0;
        unsigned     words = 0;
        unsigned     chars = 0;

        explicit Count(string name) : name{std::move(name)} {}

        explicit Count(const fs::path& file) : name{file.string()} {
            update(file);
        }

        ~Count() = default;
        Count(const Count&) = default;
        Count(Count&&) = default;

        void update(const fs::path& file) {
            chars += fs::file_size(file);

            ifstream    f{file};
            for (string line; getline(f, line);) {
                ++lines;
                istringstream buf{line};
                for (string   word; buf >> word;) ++words;
            }
        }

        void update(const Count& cnt) {
            lines += cnt.lines;
            words += cnt.words;
            chars += cnt.chars;
        }

        void operator+=(const Count& cnt) {
            update(cnt);
        }

        friend ostream& operator<<(ostream& os, const Count& cnt) {
            return os << setw(6) << cnt.lines << setw(8) << cnt.words << setw(10) << cnt.chars << "\t" << cnt.name;
        }

        static inline bool isTextFile(const fs::path& p) {
            auto name = p.string();
            if (name.find("CMake"s) != string::npos) return false;

            auto ext = p.extension().string();
            static const auto TEXT_EXT = unordered_set < string > {".txt", ".cxx", ".hxx"};
            return TEXT_EXT.count(ext);
        }

    };

}
