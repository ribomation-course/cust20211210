#include <iostream>
#include <map>

using namespace std;

auto fib(unsigned n) {
    if (n == 0U) return 0U;
    if (n == 1U) return 1U;
    return fib(n - 2) + fib(n - 1);
}

struct Pair {
    unsigned arg;
    unsigned result;

    Pair(unsigned int arg, unsigned int result)
            : arg(arg), result(result) {}
};

auto fib2(unsigned n) {
    return Pair{n, fib(n)};
}

auto compute(unsigned n) {
    auto result = map<unsigned, unsigned>{};
    for (auto k = 1U; k<=n; ++k) {
        auto [arg, f] = fib2(k);
        result[arg] = f;
    }
    return result;
}

int main() {
    auto const N = 10;
    {
        auto result = fib(N);
        cout << "fib(" << N << ") = " << result << endl;
    }
    {
        auto[arg, result] = fib2(N);
        cout << "fib(" << arg << ") = " << result << endl;
    }
    {
        for (auto [a, f] : compute(N))
            cout << "fib(" << a << ") = " << f << endl;
    }
}
