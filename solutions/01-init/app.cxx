#include <iostream>
#include <string>
#include <set>

int main() {
    using namespace std;
    using namespace std::string_literals;
    set<string> words = {
            "hello"s, "from"s, "C++"s, "hello"s
    };
    for (auto const& w: words) cout << w << endl;
}
