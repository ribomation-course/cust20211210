cmake_minimum_required(VERSION 3.16)
project(01_init)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(init app.cxx)
target_compile_options(init PRIVATE ${WARN})


