#include <iostream>
#include <stdexcept>
#include <memory>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

using std::runtime_error;
using std::cout;
using std::make_unique;
using namespace std::string_literals;

struct MemoryBlock {
    explicit MemoryBlock(unsigned long size) : size(size) {
        segment = ::mmap(0, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (segment == MAP_FAILED) {
            throw runtime_error{"mmap(2) failed: "s + strerror(errno)};
        }
    }

    ~MemoryBlock() {
        if (munmap(segment, size) < 0) {
            //throw runtime_error{"munmap(2) failed: "s + strerror(errno)};
            std::cerr << "munmap(2) failed: " << strerror(errno) << "\n";
            ::exit(1);
        }
    }

    [[nodiscard]] unsigned* asArray() const {
        return static_cast<unsigned*>(segment);
    }

private:
    void* segment;
    const unsigned long size;
};


int main() {
    auto const N    = 1'000'000UL;
    auto const size = N * sizeof(unsigned);

    auto      mem   = MemoryBlock{size};
    auto      array = mem.asArray();
    for (auto k     = 0U; k < N; ++k) array[k] = k + 1;

    auto      sum = 0L;
    for (auto k   = 0U; k < N; ++k) sum += array[k];

    cout << "sum: " << sum << "\n";
    cout << "SUM: " << (N * (N + 1) / 2) << "\n";

}
