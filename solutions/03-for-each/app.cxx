#include <iostream>
#include <string>
#include <map>
using namespace std;

int main() {
    auto f = map<string, unsigned>{};
    for (string w; cin >> w;) ++f[w];
    for (auto const& [word, count] : f)
        cout << word << ": " << count << endl;
}
