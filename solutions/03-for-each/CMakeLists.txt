cmake_minimum_required(VERSION 3.16)
project(03_for_each)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(for_each app.cxx)
target_compile_options(for_each PRIVATE ${WARN})


