#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    auto print = [](auto n) { cout << n << " "; };
    for_each(&numbers[0], &numbers[N], print);
    cout << endl;

    auto factor = 42;
    transform(begin(numbers), end(numbers), begin(numbers), [factor](auto n) {
        return n * factor;
    });

    for_each(numbers, numbers+N, print);
}
