#include <iostream>
#include "simple-data.hxx"
using namespace std;

int main() {
    char buf[sizeof(SimpleData)];
    auto ptr = new (buf) SimpleData{10};
    cout << "*ptr: " << *ptr << " @ " << ptr << "\n";

    auto p = reinterpret_cast<int*>(ptr);
    cout << "*p: " << *p << "\n";
    cout << "*p: " << *(p + 1) << "\n";
    cout << "*p: " << p[2] << "\n";
    ptr->~SimpleData();
//    delete ptr;  //this do not work!
}
