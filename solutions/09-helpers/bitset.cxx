#include <iostream>
#include <string>
#include <bitset>
using std::stoi;
using std::cout;
using std::bitset;

int main(int argc, char** argv) {
    auto a = (argc <= 1) ?  3U : stoi(argv[1]);
    auto b = (argc <= 2) ? 42U : stoi(argv[2]);
    cout << "a=" << a << ", b=" << b << "\n";

    bitset<8> A{a}, B{b};
    cout << "A=" << A << ", B=" << B << "\n";

    cout << "A & B  = " << (A & B)  << "\n";
    cout << "A | B  = " << (A | B)  << "\n";
    cout << "A ^ B  = " << (A ^ B)  << "\n";
    cout << "~A     = " << (~A)     << "\n";
    cout << "A << 2 = " << (A << 2) << "\n";
    cout << "B >> 3 = " << (B >> 3) << "\n";
}
