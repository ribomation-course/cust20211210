#include <iostream>
#include <string>
#include <random>

using namespace std;

template<typename T>
T MAXIMUM(T a, T b) { return a >= b ? a : b; }

template<typename T, typename... Args>
T MAXIMUM(T a, T b, Args... args) {
    return MAXIMUM(MAXIMUM(a, b), args...);
}

int main() {
    cout << "int   : " << MAXIMUM(5, 10, 3, 2, 12, 16, 1, 42) << endl;
    cout << "string: " << MAXIMUM("a"s, "B"s, "c"s, "D"s, "e"s, "F"s, "G"s) << endl;

    cout << "----\n";
    auto r    = random_device{};
    auto rect = uniform_int_distribution<unsigned>{10, 100};
    auto a    = rect(r);
    auto b    = rect(r);
    auto c    = rect(r);
    auto d    = rect(r);
    cout << "max(" << a << ", " << b << ", " << c << ", " << d << ") = " << MAXIMUM(a, b, c, d) << "\n";
}

