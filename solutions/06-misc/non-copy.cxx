#include <iostream>
#include <string>

using namespace std;

struct Person {
    Person(string name_, unsigned age_) : name{move(name_)}, age{age_} {}

    string getName() const {return name;}
    unsigned getAge() const {return age;}
    void setName(string name_) {name=move(name_);}
    unsigned incrAge() {return ++age;}

    Person() = default;
    Person(const Person&) = delete;
    Person& operator=(const Person&) = delete;

private:
    string   name;
    unsigned age;
};

auto operator<<(ostream& os, const Person& p) -> ostream& {
    return os << "Person{" << p.getName() << ", " << p.getAge() << "}";
}

void func(Person r) { // Person r{ arg p }
    cout << "r: " << r << "\n";
}

Person func2() {
    return {"Anna Conda", 42};
}

int main() {
    auto p = Person{"Nisse Hult"s, 42};
    cout << "p: " << p << "\n";
    // operator<<( operator<<(cout, "p: "), p)

     func(p);
    // error: use of deleted function Person::Person=(const Person&)

    auto p2 = func2();
    cout << "p2: " << p2 << "\n";

    Person q{p};
     q = p;
    // error: use of deleted function Person& Person::operator=(const Person&)
}
