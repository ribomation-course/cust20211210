#include "gtest/gtest.h"
#include "numeric-stats.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;


TEST(integral, allTen) {
    auto  data = vector<int>{10, 10, 10, 10, 10};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_EQ(avg, 10);
    EXPECT_EQ(min, 10);
    EXPECT_EQ(max, 10);
}

TEST(integral, oneToFive) {
    auto data = vector<int>{1, 2, 3, 4, 5};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_DOUBLE_EQ(avg, 3);
    EXPECT_EQ(min, 1);
    EXPECT_EQ(max, 5);
}

TEST(integral, empty) {
    auto data = vector<int>{};
    EXPECT_THROW(numeric_stats(data), invalid_argument);
}

TEST(floatingPoint, oneToFive) {
    auto data = vector<float>{1.F, 2.F, 3.F, 4.F, 5.F};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_DOUBLE_EQ(avg, 3.);
    EXPECT_EQ(min, 1.F);
    EXPECT_EQ(max, 5.F);
}

