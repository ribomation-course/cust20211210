#include <iostream>
#include "person.hxx"
using namespace std;
using namespace ribomation;

auto func(Person r) -> Person {
    cout << "[func] q: " << r << endl;
    r.incrAge();
    cout << "[func] q: " << r << endl;
    return r;
}

int main() {
    auto p = Person{"Cris P. Bacon", 27};
    cout << "[main] p: " << p << endl;

    cout << "-------\n";
    auto q = func(move(p));
    cout << "[main] p: " << p << endl;
    cout << "[main] q: " << q << endl;

    cout << "-------\n";
    p = move(q);
    cout << "[main] p: " << p << endl;
    cout << "[main] q: " << q << endl;
}
