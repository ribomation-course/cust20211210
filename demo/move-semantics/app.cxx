#include <iostream>
#include "blob.hxx"
using namespace std;

Blob mkOne() {
    Blob nob{50};
    cerr << "[mkOne] nob: " << nob << endl;
    return nob;
}

int main() {
    Blob bob{125};
    cout << "bob: " << bob << endl;

    Blob rob{move(bob)};
    cout << "bob: " << bob << endl;
    cout << "rob: " << rob << endl;

    rob = mkOne();
    cout << "bob: " << bob << endl;
    cout << "rob: " << rob << endl;

    return 0;
}

