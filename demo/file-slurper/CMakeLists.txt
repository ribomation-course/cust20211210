cmake_minimum_required(VERSION 3.10)
project(file_slurper)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(file-slurper
        file-slurper.hxx
        app.cxx
        )

